import {Form} from "./utils/form"
import {Modal} from "./utils/modal"
import {Tools} from "./utils/tools"

const tools = new Tools();
new Modal();
new Form();
